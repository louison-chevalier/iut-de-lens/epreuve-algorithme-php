<?php ?>

<!DOCTYPE html>
<html>
<head>
    <title>Hello</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="CSS/bootstrap.css">

    <!-- jQuery library -->
    <script type="text/javascript"  src="JS/jquery.js"></script>

    <!-- Latest compiled JavaScript -->
    <script type="text/javascript"  src="JS/bootsrap.js"></script>

    <script type="text/javascript"  src="JS/custom.js"></script>

    <style>
        li, h3{
            margin-top : 20px;
        }
        td{
            padding: 2px;
        }
        #nb{
            padding : 11px;
        }
        .nav{
            text-align: center;
        }
        h2{
            font-size: 14px;
        }
    </style>
</head>
<body>
<div class="container nav text-center" style="margin-bottom: 15px;">
    <h1>Épreuve du 5 octobre 2018</h1>
    <h2>By Louison Chevalier</h2>
    <ul class="nav nav-pills">
        <li><a href="partie1.php">Partie1</a></li>
        <li><a href="partie2.php">Partie 2</a></li>
    </ul>
</div>