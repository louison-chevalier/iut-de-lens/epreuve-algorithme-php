<?php require_once 'navigation.php';?>

<?php
    //Valeur par défault (on n'affiche pas la section affiche)
    $sectionaffiche = 0;


    // Si l'utilisateur envoie le formulaire
    if(isset($_POST['btAjouter'])){
        $valeur = $_POST['valeur'];
        $sectionaffiche = 1;

        $compte= 0;
        for($i = 1; $i < $valeur; $i++){
            $compte = $compte+100;
        }
        $compte = $compte+($valeur*2);
    }
?>


<div class="container well text-center">
    <form  method="post"  action="partie2.php">
        <p> Quel âge a Marie ?
            <input type="number" id="valeur" name="valeur" min="0">
            <input  type="submit" id="btAjouter" name="btAjouter" value="Ajouter">
        </p>
    </form>
    <?php
        if ($sectionaffiche == 1){
            echo "Marie ayant ".$valeur." ans, son compte en banque contient maintenant ".$compte." euros.";
        }
    ?>
</div>