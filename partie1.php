<?php require_once 'navigation.php';?>

<?php
//Valeur par défault (on n'affiche pas la section affiche)
$sectionaffiche = 0;

//Document de référence
$fichier = file('assets/change.txt');

//Intégrer les données du txt dans un tableau
$laligne = 0;

//Nombre de ligne
$nbligne = 0;

foreach ($fichier as $uneligne) {
    $lesmots = explode('	', $uneligne);
    $change[$laligne] = array($lesmots[0], $lesmots[1], $lesmots[2]);
    $laligne++;
    $nbligne++;
}

// Si l'utilisateur envoie le formulaire
if(isset($_POST['btAjouter'])){
    $valeur = $_POST['valeur'];
    $sectionaffiche = 1;
    for ($g=0; $g < $nbligne;$g++){
        $lamonaie = (float) $change[$g][0];
        $change[$g][3] = $lamonaie*$valeur;
    }
}
?>
<div class="container well text-center">
    <form  method="post"  action="partie1.php">
        <p>Saisissez une somme en euros
            <input type="number" id="valeur" name="valeur" min="0">
            <input  type="submit" id="btAjouter" name="btAjouter" value="Ajouter">
        </p>
    </form>

    <?php if ($sectionaffiche == 1){ ?>
    <table class="table table-dark">
        <thead>
            <td>Taux</td>
            <td>Diminutif</td>
            <td>Monaie</td>
            <td>Valeur</td>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>EUR</td>
                <td>Euros</td>
                <td><?php echo"$valeur"; ?></td>
            </tr>
            <?php
                foreach ($change as $value){
                    echo "<tr>";
                    echo "<td>$value[0]</td>";
                    echo "<td>$value[1]</td>";
                    echo "<td>$value[2]</td>";
                    echo "<td>$value[3]</td>";
                    echo "<tr>";
                }
            ?>
        </tbody>
    </table>
    <?php } ?>
</div>